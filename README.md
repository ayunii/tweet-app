# TWEET APP

This is a web application that use CRUD principles.\
The project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and [Material UI](https://mui.com/)

## Run the project

The web app under development process. Thus, need to run it locally by cloning the project to local machine.\
In the project directory, run the following terminal:

### `npm install`

This command will install all the packages for the app.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Data 

The data loaded to the web app are based on [JSONPlaceholder](https://jsonplaceholder.typicode.com/).\
List of APIs used are:

[https://jsonplaceholder.typicode.com/posts/](https://jsonplaceholder.typicode.com/posts/)\
[https://jsonplaceholder.typicode.com/posts/:id](https://jsonplaceholder.typicode.com/posts/1)\
[https://jsonplaceholder.typicode.com/posts/:id/comments](https://jsonplaceholder.typicode.com/posts/1/comments)

For Login, enter the following details.

Email: `test@gmail.com`\
Password: `test123`
