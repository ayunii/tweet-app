import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    isLogged: false
  },
  reducers: {
    isLoggedIn: (state) => {
      let stateValue = state.isLogged
      return {
        isLogged: !stateValue
      }
    }
  }
})

// console.log(userSlice.actions.isLoggedIn)

// const newState = userSlice.reducer(
//   { isLogged: true },
//   userSlice.actions.isLoggedIn()
// )
// console.log(newState)

export const { isLoggedIn } = userSlice.actions

export default userSlice.reducer