import API_URL from "../../APIs";

// Fetch all posts
const fetchPosts = async () => {
  const res = await fetch(`${API_URL}/posts`)
  const data = await res.json()

  //console.log(data)
  return data
}

// Fetch single post
const fetchPost = async (id) => {
  const res = await fetch(`${API_URL}/posts/${id}`)
  const data = await res.json()

  // console.log(data)
  return data
}

export { fetchPost, fetchPosts };