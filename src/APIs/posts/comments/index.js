import API_URL from "../..";

export const fetchComments = async (id) => {
  const res = await fetch(`${API_URL}/posts/${id}/comments`)
  const data = await res.json()

  // console.log(data)
  return data
}
