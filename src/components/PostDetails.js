import { useEffect, useState } from "react";
import { useLocation, useParams } from "react-router-dom";

import { Card, CardContent, Typography, Divider, Button, TextField } from "@mui/material";
import DeleteIcon from '@mui/icons-material/Delete';

import { fetchPost } from "../APIs/posts";
import { fetchComments } from "../APIs/posts/comments";

import { useSelector } from "react-redux";

const PostDetails = () => {
  const [post, setPost] = useState({})
  const [comments, setComments] = useState([])
  const [isEdit, setIsEdit] = useState(false)
  const [isPostDelete, setIsPostDelete] = useState(false)
  const isAuthenticated = useSelector(state => state.user.isLogged)

  // const params = useLocation()
  // const id = params.state.id
  // console.log('params (id of clicked post) from other page', id)

  const params = useParams()
  const id = params.id
  useEffect(() => {
    const fetchPostData = async () => {
      const tmpData = await fetchPost(id)
      setPost(tmpData)
    }
    const fetchCommentData = async () => {
      const tmpData = await fetchComments(id)
      setComments(tmpData)
    }
    fetchPostData()
    fetchCommentData()
  }, []) 

  const handleDeleteComment = (id) => {
    const newComments = comments.filter(comment => comment.id !== id)
    setComments(newComments)
  }

  return (
    <>
      { isPostDelete ? <h4 style={{marginTop: "10%"}}>You deleted the post</h4> :
      <Card sx={{ width: "65%",  marginInline: "auto" }}>
        <CardContent>

          { isAuthenticated &&
          <div style={{display: "flex", justifyContent: "space-between", marginBottom: 10}}>
            <Button variant="contained" color="info"  size="small" onClick={() => setIsEdit(!isEdit)}>Edit</Button>  
            <Button variant="contained" color="error"  size="small" onClick={() => setIsPostDelete(!isPostDelete)}>Delete</Button>
          </div> }

          { isEdit ?
          <TextField
            id="outlined-multiline-static"
            label="Title"
            multiline
            rows={4}
            type="text"
            value={post.title}
            onChange={e => setPost({...post, title: e.target.value})}
            fullWidth={true}
          />  : 

          <Typography variant="h3" component="div">
            { post.title }
          </Typography> }

          <Typography variant="body1">
            { post.body }
          </Typography> 
          
          <br />
          <Divider>Comments</Divider>
          <br />

          { 
            comments.map(comment => (
              <div key={comment.id}>
                <Typography variant="h6" align="left">
                  {comment.name} by {comment.email} 
                </Typography>
                <Typography variant="subtitle2" align="left">
                  {comment.body}
                </Typography>
                { isAuthenticated && (
                  <div style={{textAlign: "right", marginBottom: 10}}>
                    <Button variant="contained" onClick={() => handleDeleteComment(comment.id)} color="error"  size="small" startIcon={<DeleteIcon />}>
                      Delete
                    </Button>
                  </div>
                )}
                <Divider />
              </div>
            ))
          }
        </CardContent>
      </Card> }
    </>
  )
}

export default PostDetails;