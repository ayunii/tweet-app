import { useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";
import { Box, AppBar, Typography, Button, Toolbar} from '@mui/material';
import Logout from "./Logout";

import { useSelector} from "react-redux";

const Header = ({ title }) => {
  const isAuthenticated = useSelector(state => state.user.isLogged)
  const navigate = useNavigate(); 

  useEffect(() => {
    
  }, [])

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed" color="primary">
        <Toolbar>
          <Typography
            variant="h6"
            align="left"
            component={Link}
            to="/"
            sx={{
              flexGrow: 1,
              mr: 2,
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
            }}>
            {title}
          </Typography>
          
          { isAuthenticated ? 
            <Logout /> : 
            <Button color="secondary" variant="contained" onClick={() => navigate("/login")} >Login</Button>
          }
        </Toolbar>
      </AppBar>
    </Box>
  )
}

export default Header;