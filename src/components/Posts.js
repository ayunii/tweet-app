import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Box, List, ListItem, ListItemButton, ListItemText } from "@mui/material";

import { fetchPosts } from "../APIs/posts";

const Posts = () => { 
  const navigate = useNavigate()
  const [posts, setPosts] = useState([])
  
  useEffect(() => {
    const getPosts = async () => {
      const postsFromServer = await fetchPosts()
      setPosts(postsFromServer)
    }
    getPosts()
  }, [])

  return (
    <Box sx={{ flexGrow: 1, maxWidth: 752, marginInline: "auto" }}>
      {
        posts.map(post => (  
            <List key={post.id}>
              <ListItem disablePadding style={{border: "2px solid lightgrey"}}>
                <ListItemButton>
                  <ListItemText primary={post.title} secondary={post.body} onClick={() => navigate(`/details/${post.id}`)} />
                  {/* <ListItemText primary={post.title} secondary={post.body} onClick={() => navigate("/details", { state: {id: post.id} })} /> */}
                </ListItemButton>
              </ListItem>
            </List>     
        ))
      }
    </Box>
  )
}

export default Posts;