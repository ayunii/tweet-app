import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { isLoggedIn } from "../redux/sliceReducers/userSlice";
import { TextField, Button, Card, CardContent, Typography } from "@mui/material";


const users = [
  {
    id: 1, 
    email: "test@gmail.com",
    password: "test123"
  }
]

const Login = () => {
  const isAuthenticated = useSelector(state => state.user.isLogged)
  const dispatch = useDispatch()
  const navigate = useNavigate(); 

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [user, setUser] = useState()

  useEffect(() => {
    
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault()

    if (!email && !password) return alert("Please enter login details")
    if (!email) return alert("Please provide an email")
    if (!password) return alert("Please enter the password")

    if (email === users[0].email && password === users[0].password) {
      setUser(users[0])
      dispatch(isLoggedIn()) // Update global state
    }
    else return alert(`No ${email} registered `)
  }

  return (
    <>
      { isAuthenticated ? 
        <>
          <h4 style={{marginTop: "10%"}}>{user.email} have logged in to the system</h4>
          <Button variant="contained" onClick={() => navigate("/")}>Main Page</Button>
        </> :

        <Card sx={{ width: "40%",  marginInline: "auto", paddingBlock: 10 }}>
          <Typography sx={{ paddingBottom: 2}} variant="h4" component="div">LOGIN</Typography>
          <CardContent>
            <div >
              <TextField sx={{ width: "80%", margin: "auto" }} id="outlined-basic" label="Email" variant="outlined" 
                type="email" value={email} onChange={e => setEmail(e.target.value)} />
            </div>
            <br />
            <div>
              <TextField sx={{ width: "80%", margin: "auto" }} id="outlined-basic" label="Password" variant="outlined" 
                type="password" value={password} onChange={e => setPassword(e.target.value)} />
            </div>
            <br />
            <Button sx={{ width: "80%", margin: "auto" }} variant="contained" onClick={handleSubmit}>Login</Button>
          </CardContent>
        </Card> }
    </>
  )
}

export default Login;