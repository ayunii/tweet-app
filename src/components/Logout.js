import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { isLoggedIn } from "../redux/sliceReducers/userSlice";

const Logout = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate(); 

  const handleLogout = () => {
    dispatch(isLoggedIn())

    const path = `/`; 
    navigate(path);
  }

  return (
    <Button color="secondary" variant="contained" onClick={handleLogout} >Logout</Button> 
  )
}

export default Logout