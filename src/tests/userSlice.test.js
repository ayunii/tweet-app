import reducer, { isLoggedIn } from '../redux/sliceReducers/userSlice'

test('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual(
    {
      isLogged: false
    }
  )
})

test('should change initial state', () => {
  const previousState = false
  expect(reducer(previousState, isLoggedIn(true))).toEqual(
    {
      isLogged: !previousState
    }
  )
})
