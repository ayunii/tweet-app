import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import {render, fireEvent, waitFor, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
import Posts from '../components/Posts';
import API_URL from '../APIs';

const server = setupServer(
  rest.get(`${API_URL}/posts`, (req, res, ctx) => {
    return res(ctx.json())
  }),
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('handlers server error', async () => {
  server.use(
    rest.get(`${API_URL}/posts`, (req, res, ctx) => {
      return res(ctx.status(500))
    })
  )

  render(<Router><Posts /></Router>)
})

