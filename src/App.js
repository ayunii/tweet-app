import { useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import './App.css';
import Header from "./components/Header";
import PostDetails from "./components/PostDetails";
import Posts from "./components/Posts";
import Login from "./components/Login";


function App() {

  useEffect(() => {

  }, [])

  return (
    <BrowserRouter>
      <div className="App">
        <Header title="TweetForum"  />
        <div style={{ marginTop: "10%" }}></div>


        <Routes>
          <Route exact path="" element={<Posts />} />
          <Route path="/details" element={<PostDetails />} />
          <Route path="/details/:id" element={<PostDetails />} />
          <Route path="/login" element={<Login />} />
        </Routes>


      </div>
    </BrowserRouter>
  );
}

export default App;
